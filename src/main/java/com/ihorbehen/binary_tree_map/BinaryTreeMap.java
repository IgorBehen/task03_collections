package com.ihorbehen.binary_tree_map;

import java.util.*;

public class BinaryTreeMap<K, V> implements Map<K, V> {

    Optional<Node<K, V>> root = Optional.empty();

    @Override
    public V put(final K key, final V value) {
        if (root.isPresent()) {
            return root.get().put(key, value);
        } else {
            root = Node.createNode(key, value);
            return null;
        }
    }

    @Override
    public V get(final Object key) {
        if (root.isPresent()) {
            return root.get().get(key);
        } else {
            return null;
        }
    }

    @Override
    public int size() {
        if (root.isPresent()) {
            return root.get().size();
        } else {
            return 0;
        }
    }

    @Override
    public boolean isEmpty() {
        return !root.isPresent();
    }

    @Override
    public boolean containsKey(final Object key) {
        final Set<K> keys = keySet();
        return keys.contains(key);
    }

    @Override
    public boolean containsValue(final Object value) {
        final Collection<V> keys = values();
        return keys.contains(value);
    }

    @Override
    public V remove(final Object key) {
        if (root.isPresent()) {
            return root.get().remove(key);
        } else {
            return null;
        }
    }

    @Override
    public void putAll(final Map<? extends K, ? extends V> m) {
        for (final Map.Entry<? extends K, ? extends V> e : m.entrySet()) {
            put(e.getKey(), e.getValue());
        }
    }

    @Override
    public void clear() {
        this.root = Optional.empty();
    }

    @Override
    public Set<K> keySet() {
        final Set<Entry<K, V>> entries = entrySet();
        final Set<K> keys = new HashSet<>(entries.size());
        for (final Map.Entry<K, V> entry : entries) {
            keys.add(entry.getKey());
        }
        return keys;
    }

    @Override
    public Collection<V> values() {
        final Set<Entry<K, V>> entries = entrySet();
        final List<V> values = new ArrayList<>(entries.size());
        for (final Map.Entry<K, V> entry : entries) {
            values.add(entry.getValue());
        }
        return values;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        if (root.isPresent()) {
            final Set<Entry<K, V>> set = new HashSet<>();
            root.get().addAllEntries(set);
            return set;
        } else {
            return new HashSet<>();
        }
    }

    @Override
    public String toString() {
        return "Tree{" + (root.isPresent() ? root.get().toString() : "") + "}";
    }
}