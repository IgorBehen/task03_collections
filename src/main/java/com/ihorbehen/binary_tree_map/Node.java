package com.ihorbehen.binary_tree_map;

import java.util.*;

class Node<K, V> {
    Optional<Node<K, V>> left = Optional.empty();
    Optional<Node<K, V>> right = Optional.empty();
    final int id;
    List<Entry<K, V>> entries = new LinkedList<>();

    public Node(final int id) {
        this.id = id;
    }

    public int size() {
        int leftSize = 0;
        int rightSize = 0;

        if (left.isPresent()) {
            leftSize = left.get().size();
        }
        if (right.isPresent()) {
            rightSize = right.get().size();
        }
        return entries.size() + leftSize + rightSize;
    }

    public V put(final K key, final V value) {
        V prevVal = null;
        final int idKey = key.hashCode();
        if (id == idKey) {
            final Iterator<Entry<K, V>> it = entries.iterator();
            while (it.hasNext()) {
                final Entry<K, V> e = it.next();
                if (e.getKey().equals(key)) {
                    prevVal = e.getValue();
                    it.remove();
                }
            }
            entries.add(new Entry<>(key, value));
        } else if (idKey < id) {
            if (left.isPresent()) {
                prevVal = left.get().put(key, value);
            } else {
                left = createNode(key, value);
                prevVal = null;
            }
        } else if (idKey > id) {
            if (right.isPresent()) {
                prevVal = right.get().put(key, value);
            } else {
                right = createNode(key, value);
                prevVal = null;
            }
        }
        return prevVal;
    }

    public V get(final Object key) {
        final int idKey = key.hashCode();
        if (id == idKey) {
            final Iterator<Entry<K, V>> it = entries.iterator();
            while (it.hasNext()) {
                final Entry<K, V> e = it.next();
                if (e.getKey().equals(key)) {
                    return e.getValue();
                }
            }
            return null;
        } else if (idKey < id) {
            if (left.isPresent()) {
                return left.get().get(key);
            } else {
                return null;
            }
        } else {
            if (right.isPresent()) {
                return right.get().get(key);
            } else {
                return null;
            }
        }
    }

    public V remove(final Object key) {
        final int keyId = key.hashCode();
        if (keyId == id) {
            final Iterator<Entry<K, V>> it = entries.iterator();
            while (it.hasNext()) {
                final Entry<K, V> e = it.next();
                if (e.getKey().equals(key)) {
                    it.remove();
                    return e.getValue();
                }
            }
            return null;
        } else if (keyId < id) {
            if (left.isPresent()) {
                return left.get().remove(key);
            } else {
                return null;
            }
        } else {
            if (right.isPresent()) {
                return right.get().remove(key);
            } else {
                return null;
            }
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for (final Entry<K, V> e : entries) {
            sb.append(e).append(',');
        }
        return (left.isPresent() ? left.get().toString() : "")
                + sb.toString()
                + (right.isPresent() ? right.get().toString() : "");
    }

    public static <K, V> Optional<Node<K, V>> createNode(final K key,
                                                         final V value) {
        final Node<K, V> node = new Node<>(key.hashCode());
        node.entries.add(new Entry<>(key, value));
        return Optional.of(node);
    }

    public void addAllEntries(final Collection<Map.Entry<K, V>> s) {
        s.addAll(entries);
        if (left.isPresent()) {
            left.get().addAllEntries(s);
        }
        if (right.isPresent()) {
            right.get().addAllEntries(s);
        }
    }
}