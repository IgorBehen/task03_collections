package com.ihorbehen.deque;


import java.util.ArrayDeque;
import java.util.Deque;

public class Main {
    Deque deque = new ArrayDeque();

    public static void main(String[] args) {
        MyDeque<Droid> myDeque = new MyDeque<>();
        Droid droidFirst = new Droid(100, "R2-D2");
        Droid droidSecond = new Droid(40, "C-3PO");
        Droid droidThird = new Droid(70, "IG-88");
        Droid droidFourth = new Droid(90, "IG-99");
        myDeque.addFirsToMyDeque(droidThird);
        myDeque.addFirsToMyDeque(droidFourth);
        myDeque.addLastToMyDeque(droidSecond);
        myDeque.addLastToMyDeque(droidFirst);
        myDeque.addFirsToMyDeque(droidSecond);

        System.out.println("All list: ");
        System.out.println(myDeque.toString());

        System.out.println("Size Of My Deque: " + myDeque.sizeOfMyDeque());
        System.out.println();

        myDeque.peekFirstFromMyDeque();
        System.out.println("List after peek first : ");
        System.out.println(myDeque.toString());

        myDeque.peekLastFromMyDeque();
        System.out.println("List after peek last: ");
        System.out.println(myDeque.toString());

        myDeque.removeLastFromMyDeque();
        System.out.println("Without last: ");
        System.out.println(myDeque.toString());

        myDeque.removeFirstFromMyDeque();
        System.out.println("Without first: ");
        System.out.println(myDeque.toString());

        myDeque.removeAllFromMyDeque();
        System.out.println("Remove all:  ");
        System.out.println(myDeque.toString());

        System.out.println("Size Of My Deque: " + myDeque.sizeOfMyDeque());
    }
}
