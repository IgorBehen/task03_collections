package com.ihorbehen.deque;

import java.util.ArrayList;
import java.util.List;

public class MyDeque<T> {
    List<T> list = new ArrayList<T>();

    private Object getLastElemFromMyDeque(List<T> list) {
        int last = list.size() - 1;
        return list.get(last);
    }

    void addLastToMyDeque(Object o) {
        System.out.println("add last: " + o);
        System.out.println();
        list.add((T) o);

    }

    void addFirsToMyDeque(Object o) {
        System.out.println("add first: " + o);
        System.out.println();
        list.add(0, (T) o);
    }

    void removeLastFromMyDeque() {
        Object o = getLastElemFromMyDeque(list);
        System.out.println("removed last: " + o);
        System.out.println();
        list.remove(o);
    }

    void removeFirstFromMyDeque() {
        System.out.println("removed first: " + list.get(0));
        System.out.println();
        list.remove(0);
    }

    void peekFirstFromMyDeque() {
        System.out.println("peek first: " + list.get(0));
        System.out.println();
    }

    void peekLastFromMyDeque() {
        Object o = getLastElemFromMyDeque(list);
        System.out.println("peek last: " + o);
        System.out.println();
    }

    void removeAllFromMyDeque() {
        for (int i = 0; i < list.size(); i++) {
            list.remove(i);
        }
        list.remove(list.size() - 1);
    }

    int sizeOfMyDeque() {
        return list.size();
    }

    @Override
    public String toString() {
        String s = "";
        int count = 0;
        for (T t : list) {
            count++;
            System.out.println(count + ". " + t);
        }
        return s;
    }

}
