package com.ihorbehen.priority_queue;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MyPriorityQueue<T> {

    private List<T> list;

    private MyPriorityQueue(ArrayList<T> list) {
        this.list = list;
    }

    private void pollFromMyPrQ(List<T> list) { //poll()
        System.out.println(list.get(0));
        list.remove(0);
    }

    @Override
    public String toString() {
        String s = "";
        for (T t : list) {
            System.out.println(t);
        }
        return s;
    }

    public static void main(String[] args) {
        Droid droidFirst = new Droid(100, "R2-D2");
        Droid droidSecond = new Droid(40, "C-3PO");
        Droid droidThird = new Droid(70, "IG-88");
        ArrayList list1 = new ArrayList();
        list1.add(droidFirst);
        list1.add(droidSecond);
        list1.add(droidThird);
        list1.sort(Comparator.comparing(Droid::getName));
        MyPriorityQueue<Droid> myPriorityQueuey = new MyPriorityQueue<Droid>(list1);
        System.out.println(myPriorityQueuey.toString());
        myPriorityQueuey.pollFromMyPrQ(list1);
        System.out.println();
        System.out.println(myPriorityQueuey.toString());
    }
}
